package main

import (
    "fmt"
    "net/http"
)

func hello(w http.ResponseWriter, req *http.Request) {

    fmt.Fprintf(w, "hello\n")
}

func headers(w http.ResponseWriter, req *http.Request) {

    for name, headers := range req.Header {
        for _, h := range headers {
            fmt.Fprintf(w, "%v: %v\n", name, h)
        }
    }
}

func main() {
    fmt.Print("register endpoint /hello\n")
    http.HandleFunc("/hello", hello)
    fmt.Print("register endpoint /headers\n")
    http.HandleFunc("/headers", headers)

    fmt.Print("Starting Go http server listening on port 8090...\n")
    http.ListenAndServe(":8090", nil)

}
